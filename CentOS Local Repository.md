בגלל ש`natasha-gw` עושה בעיות מצאתי דרך להתקין local repository שמאפשר להתקין דברים מהISO של centOS עם פקודת `yum`. <br> קחו בחשבון שאם תעשו את זה אחר כך לא תוכלו להתקין דברים מהאינטרנט בלי להחזיר חזרה דברים שתשנו בתהליך.

קודם כל מחברים את הISO של CentOS 7 לכונן דיסקים ומוודאים שהוא connected. אחרי זה עושים mount כמו שכבר עשינו:
<br>(אפשר לוודא מה הנתיב הנכון באמצעות `lsblk`. במכונות שלנו זה תמיד `/dev/sr0`)
```bash
mount /dev/sr0 /mnt
```
עכשיו יוצרים תיקייה ומעתיקים לתוכה את התוכן של הISO:
```bash
mkdir /install
cp -r /mnt /install/yum
```
עכשיו מתקינים מתוך הספריה הזאת פקודה שנקראת `createrepo` ועוד כמה דברים שהיא צריכה כדי לפעול:
```bash
cd /install/yum/Packages
rpm -ivh deltarpm-3.6-3.el7.x86_64.rpm
rpm -ivh libxml2-python-2.9.1-6.el7.4.x86_64.rpm
rpm -ivh python-deltarpm-3.6-3.el7.x86_64.rpm
rpm -ivh createrepo-0.9.9-28.el7.noarch.rpm
```
מפעילים את הפקודה:
```bash
createrepo /install
```
בשלב הבא נחליף את השם של הקבצים הקיימים שמהם `yum` לוקח את הספריות שלו. (זה השלב שחובה להפוך אם תרצו בהמשך להתקין דברים מהרשת):
```bash
cd /etc/yum.repos.d
find . -name "*.repo" -exec mv {} {}.bak \;
```
ניצור קובץ חדש שמכיל את ההגדרות לספריה המקומית שלנו:
```bash
vi local.repo
```
ובתוכו נכתוב:
```bash
[local]
name=yum
baseurl=file:///install
```
הדבר האחרון שנעשה הוא לבטל את הדרישה של `yum` לאמת את הקבצים באמצעות חתימת GPG:
```bash
vi /etc/yum.conf
```
ובתוך הקובץ נחליף מ1 ל0:
```bash
gpgcheck=0
```

ברכותי, כעת אתם יכולים להתקין כל מה שקיים בISO. <br>
במקרה שלנו השתמשתי בזה כדי להתקין `httpd` על הWeb Server:
```bash
yum install httpd -y
systemctl start httpd
```
ולא לשכוח לכבות את הפיירוול:
```bash
systemctl stop firewalld
systemctl disable firewalld
```
עכשיו תוכלו להשתמש בPC Beta כדי לשלוח בקשת http לשרת:
```bash
curl 192.168.12.80
```
